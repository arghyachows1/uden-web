import React from 'react'
import './Register.scss'
import facebook from '../../assets/facebook.svg'
import google from '../../assets/google.svg'
import linkedin from '../../assets/linkedin.svg'
import { connect } from 'react-redux'
import { createUser } from './actions'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

const { useState } = React

const Register = ({ onPageChange, page, onCreatePressed }) => {

    const [changeType, setchangeType] = useState('Individual')
    const [user, setUser] = useState({})

    const schema = yup.object().shape({
        name: yup.string().required().max(30),
        email: yup.string().email().required(),
        username: yup.string().required().min(8).max(15),
        password: yup.string().required().min(8).max(15),
    });

    const { register, handleSubmit, errors } = useForm({
        resolver: yupResolver(schema)
    });

    const onSubmit = (data) => {
        console.log(data)
        onCreatePressed(data)
        onPageChange();
    }

    return (
        <div className="col-md-12">
            <div className="row">
                <div className="col-md-12">
                    <h4 className="text-center pt-2 pb-2">Create an Account</h4>
                </div>
                <div className="col-md-12 text-center">
                    <div className="row">
                        <div className="col-md-4">
                            <button className={`btn btn-block font-weight-bold ${changeType === 'Individual' ? 'btn-warning' : ''} `} onClick={() => {
                                setchangeType('Individual')
                            }}>Individual</button>
                        </div>
                        <div className="col-md-4">
                            <button className={`btn btn-block font-weight-bold ${changeType === 'Corporate' ? 'btn-primary' : ''} `} onClick={() => {
                                setchangeType('Corporate')
                            }} >Corporate</button>
                        </div>
                        <div className="col-md-4">
                            <button className={`btn btn-block font-weight-bold ${changeType === 'Educator' ? 'btn-danger' : ''} `} onClick={() => {
                                setchangeType('Educator')
                            }} >Educator</button>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 text-center">
                    <img src={facebook} alt="facebook" className="social-icon" />
                    <img src={google} alt="google" className="social-icon" />
                    <img src={linkedin} alt="linkedin" className="social-icon" />
                </div>
                <div className="col-md-12 text-center">
                    <a className="email-link" href="/">Or use your Email instead</a>
                </div>

                <div className="col-md-4 mx-auto">
                    <form onSubmit={handleSubmit(onSubmit)}>

                        <div className="form-group">
                            <label htmlFor="name" className="font-weight-bold">Name</label>
                            <input type="text" className="form-control" name="name" ref={register({ required: true })} />
                            <span className="val-error">{errors.name?.message}</span>
                        </div>
                        <div className="form-group">
                            <label htmlFor="email" className="font-weight-bold">Email</label>
                            <input type="email" className="form-control" name="email" ref={register({ required: true })} />
                            <span className="val-error">{errors.email?.message}</span>
                        </div>
                        <div className="form-group">
                            <label htmlFor="username" className="font-weight-bold">Username</label>
                            <input type="text" className="form-control" name="username" ref={register({ required: true })} />
                            <span className="val-error">{errors.username?.message}</span>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password" className="font-weight-bold">Password</label>
                            <input type="password" className="form-control" name="password" ref={register({ required: true })} />
                            <span className="val-error">{errors.password?.message}</span>
                        </div>
                        <div className="form-group">
                            <p className="small">Make sure it’s <span className={`${changeType === 'Individual' ? 'text-warning' : changeType === 'Corporate' ? 'text-primary' : 'text-danger'}`}>at least 15 characters</span> OR at least 8 characters including a number and a lowercase letter. <a href="/">Learn More</a></p>
                        </div>

                        <button type="submit" className={`btn btn-block ${changeType === 'Individual' ? 'btn-warning' : changeType === 'Corporate' ? 'btn-primary' : 'btn-danger'}`}>Create Account!</button>
                    </form>

                </div>

                <div className="col-md-12 mx-auto">
                    <div className="row">
                        <div className="col-md-4 mx-auto text-center pt-2 small">
                            <a href="/">Already a user? Sign In</a>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 mx-auto">
                    <div className="row">
                        <div className="col-md-4 mx-auto pt-2 small">
                            By creating an account, you agree to the <a href="/">Terms of Service</a>. For more
                                information about GitHub’s privacy practices, see the <a href="/">UDEN Privacy
                                Statement</a>. We’ll occasionally send you account-related emails.
                            </div>
                    </div>
                </div>

            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    user: state.user
})

const mapDispatchToProps = dispatch => ({
    onCreatePressed: text => dispatch(createUser(text))
})

export default connect(mapStateToProps, mapDispatchToProps)(Register);
