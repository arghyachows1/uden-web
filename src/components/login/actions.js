
export const CREATE_USER = 'CREATE_USER'
export const createUser = (payload) => ({
    type: CREATE_USER,
    payload
})

export const UPDATE_USER = 'UPDATE_USER'
export const updateUser = (payload) => ({
    type: UPDATE_USER,
    payload
})
