import { createStore, combineReducers } from 'redux';
import { user } from './components/login/reducers'

const reducers = {
    user
};

const rootReducer = combineReducers(reducers);

export const configureStore = () => createStore(rootReducer);
